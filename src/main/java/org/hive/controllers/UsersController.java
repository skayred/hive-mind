package org.hive.controllers;

import org.hive.hosting.HiveController;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 6/12/13
 * Time: 8:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class UsersController extends HiveController {
    public String index() {
        return renderTemplate("users/get");
    }
}
