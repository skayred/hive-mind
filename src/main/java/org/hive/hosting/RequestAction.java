package org.hive.hosting;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 6/12/13
 * Time: 9:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class RequestAction {
    public static final int UNKNOWN_ACTION = -1;
    public static final int INDEX = 0;
    public static final int NEW = 1;
    public static final int CREATE = 2;
    public static final int SHOW = 3;
    public static final int EDIT = 4;
    public static final int UPDATE = 5;
    public static final int DESTROY = 6;

    private int action;
    private Method method;

    private static Map<String, Integer> availableActions = new HashMap<String, Integer>() {{
        put("index", INDEX);
        put("newEntity", NEW);
        put("create", CREATE);
        put("show", SHOW);
        put("edit", EDIT);
        put("update", UPDATE);
        put("destroy", DESTROY);
    }};

    private RequestAction(int action, Method method) {
        this.action = action;
        this.method = method;
    }

    public int getAction() {
        return action;
    }

    public Method getMethod() {
        return method;
    }

    public static RequestAction createRequestAction(Method method) {
        String methodName = method.getName();

        if (!availableActions.containsKey(methodName)) {
            return null;
        }

        return new RequestAction(availableActions.get(methodName), method);
    }
}
