package org.hive.hosting;

import io.netty.handler.codec.http.HttpMethod;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 6/12/13
 * Time: 9:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class Resource {
    private String name;
    private HiveController controller;
    private Map<Integer, RequestAction> availableMethods;

    public Resource(String name, Class<? extends HiveController> controllerClass) {
        try {
            this.name = name;
            availableMethods = new HashMap<Integer, RequestAction>();

            Constructor<? extends HiveController> ctor = controllerClass.getConstructor();
            controller = ctor.newInstance();

            Method[] methodList = controllerClass.getMethods();

            for (Method method : methodList) {
                RequestAction methodAction = RequestAction.createRequestAction(method);

                if (methodAction != null) {
                    availableMethods.put(methodAction.getAction(), methodAction);
                }
            }
        } catch (Exception e) {
            //  It should not happen
            e.printStackTrace();
        }
    }

    public String getName() {
        return name;
    }

    /**
     * GET /res - index
     * GET /res/new - newEntity
     * POST /res - create
     * GET /res/:id - show
     * GET /res/:id/edit - edit
     * PUT /res/:id - update
     * DELETE /res/:id - destroy
     */
    private int getAction(HttpMethod method, String path) {
        if (method.equals(HttpMethod.GET)) {
            if (path.endsWith("new")) {
                return RequestAction.NEW;
            }
            if (path.endsWith("edit")) {
                return RequestAction.EDIT;
            }
            if (path.endsWith(name)) {
                return RequestAction.INDEX;
            } else {
                return RequestAction.SHOW;
            }
        }

        if (method.equals(HttpMethod.POST)) {
            return RequestAction.CREATE;
        }
        if (method.equals(HttpMethod.PUT)) {
            return RequestAction.UPDATE;
        }
        if (method.equals(HttpMethod.DELETE)) {
            return RequestAction.DESTROY;
        }

        return RequestAction.UNKNOWN_ACTION;
    }

    public String invokeAction(HttpMethod httpMethod, String path) throws InvocationTargetException, IllegalAccessException {
        int action = getAction(httpMethod, path);

        if (!canHandle(action)) {
            //  TODO: 404
            return "";
        }

        RequestAction actionObject = availableMethods.get(action);
        return (String) actionObject.getMethod().invoke(controller);
    }

    public boolean canHandle(int action) {
        return availableMethods.keySet().contains(action);
    }
}
