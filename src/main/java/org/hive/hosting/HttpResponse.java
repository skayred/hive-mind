package org.hive.hosting;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 6/12/13
 * Time: 10:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class HttpResponse {
    private String contentType;
    private String content;

    public HttpResponse(String content, String contentType) {
        this.content = content;
        this.contentType = contentType;
    }

    public HttpResponse(String content) {
        this(content, "text/html; charset=UTF-8");
    }

    public String getContent() {
        return content;
    }

    public String getContentType() {
        return contentType;
    }
}
