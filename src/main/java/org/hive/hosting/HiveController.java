package org.hive.hosting;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 6/12/13
 * Time: 8:53 PM
 * To change this template use File | Settings | File Templates.
 */

import de.neuland.jade4j.Jade4J;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *  Methods:
 *      - index
 *      - new
 *      - create
 *      - show
 *      - edit
 *      - update
 *      - destroy
 * */

public abstract class HiveController {
    private static final String viewsDirectory = "/Users/dmitrysavchenko/projects/netty-upload/views";

    protected String renderTemplate(String templateName) {
        return renderTemplate(templateName, new HashMap<String, Object>());
    }

    protected String renderTemplate(String templateName, Map<String, Object> params) {
        File view = new File(viewsDirectory + "/" + templateName + ".jade");

        try {
            return Jade4J.render(view.getAbsolutePath(), params);
        } catch (IOException ie) {
//              Do nothing - return 404 in future
            return "";
        }
    }
}
